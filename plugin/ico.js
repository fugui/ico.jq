/*	基于jQuery的图标插件  ico.js
----------------------------
版本：		v0.2
更新时间：	2015年9月7日
作者：		赵欣科
----------------------------
*/
(function ($) {
    $.fn.extend({
        "ico": function (options) {
            var opts = $.extend({}, defaluts, options);
			var color = opts.color;
			var b_color = opts.b_color;
			var size = opts.size;
			var type = opts.type;
			var pos = opts.pos;
			var radius = opts.radius;
			var line_h = opts.line_h;
			var space = opts.space;
			var t_size = opts.t_size;
			//var item = $(this).children();
			var item = $(this).find("[data-ico]");
			item.each(function(){
				//把通用设置备份
				var c = color;
				var bc = b_color;
				var s = size;
				var t = type;
				var p = pos;
				var r = radius;
				var l = line_h;
				var sp = space;
				var ts = t_size;
				var this_opts = $(this).data("ico");
				var options_item;
				var length;
				if(this_opts !== undefined){
					options_item = this_opts.split(",");
					length = options_item.length;
					for(i=0;i<length;i++){
						var item = options_item[i].split(":");
						var key = item[0];
						var value = item[1];
						switch(key){
							case "color":
							c = value;
							break;
							case "b_color":
							bc = value;
							break;
							case "size":
							s = value;
							break;
							case "type":
							t = value;
							break;
							case "pos":
							p = value;
							break;
							case "radius":
							r = value;
							break;
							case "line_h":
							l = value;
							break;
							case "space":
							sp = value;
							break;
							case "t_size":
							ts = value;
							break;
						}
					}
				}
				var color_col = c*s*(-1)+"px";
				var type_row = t*s*(-1)+"px";
				var padding;
				var pos_d;
				var align;
				var lh;
				var spacing = parseInt(s)+parseInt(sp);
				var min_w;
				var min_h;
				switch(pos){
					case "left":
					padding = "0 0 0 "+spacing+"px";
					pos_d = "left: 0; top:50%;margin-top:"+(s/2)*(-1)+"px";
					align = "left";
					lh = s+"px";
					min_w = "auto";
					min_h = s+"px";
					break;
					case "right":
					padding = "0 "+spacing+"px "+"0 0";
					pos_d = "right: 0; top:50%;margin-top:"+(s/2)*(-1)+"px";
					align = "right";
					lh = s+"px";
					min_w = "auto";
					min_h = s+"px";
					break;
					case "top":
					padding = spacing+"px 0 0 0";
					pos_d = "left: 50%; top:0;margin-left:"+(s/2)*(-1)+"px";
					align = "center";
					lh = l+"px";
					min_w = s+"px";
					min_h = "auto";
					break;
					case "bottom":
					padding = "0 0 "+spacing+"px 0";
					pos_d = "left: 50%; bottom:0;margin-left:"+(s/2)*(-1)+"px";
					align = "center";
					lh = l+"px";
					min_w = s+"px";
					min_h = "auto";
					break;
				}
				$(this).append("<i style='"+pos_d+"'></i>");
				$(this).css({
					"position": "relative",
					"display": "block",
					"height": lh+"px",
					"padding": padding,
					"line-height": lh,
					"text-align": align,
					"font-size": ts+"px",
					"min-width": min_w,
					"min-height": min_h,
				})
				$(this).children("i").css({
					"position": "absolute",
					"display": "block",
					"height": s+"px",
					"width": s+"px",
					"background-color": bc,
					"border-radius":r+"%",
					"background-position": color_col+" "+type_row,
				})
			})
		}
		
    });
	var defaluts = {
		size:16,
		color:1,
		b_color:"transparent",//默认透明，可以是一个16进制的颜色码
		type:0,
		pos:"left",
		radius: 0,
		line_h: 16,
		space: 5,
		t_size: 14,
	}; 
})(window.jQuery);